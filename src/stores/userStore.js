import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { supabase } from '../supabase';

export const useUserStore = defineStore('users', () => {
  const user = ref(null)
  const errorMessage = ref("");
  const loading = ref(false);
  const loadingUser = ref(false);

  const validateEmail = (email) => {
    return String(email)
      .toLowerCase()
      .match(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      );
  };

  const handleLogin = async (credentials) => {
    const { email, password } = credentials;

    if(!validateEmail(email)) {
      return errorMessage.value = "Please fill valid email"
    }

    if(!password.length) {
      return errorMessage.value = "Password cannot be empty"
    }

    loading.value = true;

    const {data, error} = await supabase.auth.signInWithPassword({
      email,
      password
    });

    if(error) {
      loading.value = false;
      return errorMessage.value = error
    }

    const {data: userDetail} = await supabase
      .from("users_detail")
      .select()
      .eq('email', email)
      .single();
    
    user.value = {
      id: userDetail.id,
      email: userDetail.email,
      username: userDetail.username
    }

    loading.value = false;
  }

  const handleSignUp = async (credentials) => {
    const { email, username, password } = credentials;

    if(password.length < 6) {
      return errorMessage.value = "Password length minimum is 6 character"
    }

    if(username.length < 4) {
      return errorMessage.value = "Username length minimum is 4 character"
    }

    if(!validateEmail(email)) {
      return errorMessage.value = "Please fill valid email"
    }

    //validate if user exist
    loading.value = true;
    const {data: isUsernameExists} = await supabase
      .from("users_detail")
      .select()
      .eq('username', username)
      .single();

    if(isUsernameExists) {
      loading.value = false;
      return errorMessage.value = "username is already exists";
    }

    const {error} = await supabase.auth.signUp({
      email,
      password
    });

    if(error) {
      loading.value = false;
      return errorMessage.value = error
    }
    
    await supabase.from("users_detail").insert({
      username,
      email
    });

    const {data: newUser} = await supabase
      .from("users_detail")
      .select()
      .eq('email', email)
      .single();
    
    user.value = {
      id: newUser.id,
      email: newUser.email,
      username: newUser.username
    }

    loading.value = false;
  }

  const clearErrorMessage = () =>  {
    errorMessage.value = '';
  }

  const handleLogout = async () => {
    loadingUser.value = true;
    await supabase.auth.signOut();
    user.value = null;
    loadingUser.value = false;
  }

  const getUser = async () => {
    loadingUser.value = true;
    const {data} = await supabase.auth.getUser();

    if(!data.user) {
      loadingUser.value = false;
      return user.value = null;
    }

    if(data) {
      const { data: userDetail } = await supabase
        .from("users_detail")
        .select()
        .eq('email', data.user.email)
        .single();

      user.value = {
        username: userDetail.username,
        email: userDetail.email,
        id: userDetail.id,
      }
    }

    loadingUser.value = false;  
  }

  return { 
    user, 
    errorMessage, 
    loading,
    loadingUser,
    handleLogin, 
    handleSignUp, 
    handleLogout, 
    getUser, 
    clearErrorMessage 
  }
})
